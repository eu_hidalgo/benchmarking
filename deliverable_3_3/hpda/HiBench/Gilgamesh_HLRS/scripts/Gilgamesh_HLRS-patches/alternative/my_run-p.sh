#!/bin/bash

HIBENCH_ROOT="{{temp_folder}}/HiBench" # Full path

$HIBENCH_ROOT/bin/prepare-all.sh

for i in {0..{{tests_repeat_count - 1}}}; do

###	hdfs dfs -rm -r -skipTrash HiBench
	rm -rf $HIBENCH_ROOT/report

	$HIBENCH_ROOT/bin/run-all-np.sh

	mv "$HIBENCH_ROOT/report" "$HIBENCH_ROOT/report_`date +"%F_%H-%M-%S"`"

done

